---
title: Multifacette - Agatha
subtitle: Ce qu'Erica fait le jeudi
author: multi.coop
date: 12 mars 2023
title-slide-attributes:
    data-background-image: "static/logo.svg, static/logo-agatha-couleurs.png"
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# Agatha ? Christie ?

:::::::::::::: {.columns}
::: {.column width="50%"}
- Déesse(s) grecque(s) de la **Bonne** (Agatha) **Fortune/Chance** (Tyché) 

- Équivalent romain **Fortuna**

- Note : Ne mettez pas des accents dans vos noms d'entreprises, c'est trop disruptif pour l'État.
::: 

::: {.column width="50%"}
![Trois Tychés - Louvre](images/tyches.jpg)
:::
::::::::::::::

# Côté Tech 

:::::::::::::: {.columns}
::: {.column width="50%"}
[![Github](images/github.png)](https://github.com/orgs/agatha-budget/repositories)
::: 

::: {.column width="50%"}
- Licence GNU GPL 3
- [Backend](https://github.com/agatha-budget/back) : Kotlin/Javalin/Jooq - **Architecture Hexagonale***
- [Frontend](https://github.com/agatha-budget/front-web) : Typescript/Vue - **Design atomique***
- [Server](https://github.com/agatha-budget/server) : Nixos
- Gestion des utilisateurs : **Keycloak*** 
- [Site vitrine](https://github.com/agatha-budget/website) : Jekyll
:::
::::::::::::::
\*multifacette à venir

# Côté utilisateur 

## Ne suivez plus vos dépenses, guidez les !

Inspiré de [YNAB](https://www.ynab.com/)
![methode agatha](images/methode.png)

[Guides disponibles en ligne](https://agatha-budget.fr/agatha/2021/12/14/starter-default/)

# Démo 

- [beta.agatha-budget.fr](https://beta.agatha-budget.fr/)
- [mon.agatha-budget.fr](https://mon.agatha-budget.fr/)